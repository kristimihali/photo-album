package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.model.AppUser;

import javax.persistence.TypedQuery;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 */
@Path("/users")
public class UsersDao extends GenericJpaRestDao<AppUser> {

	public UsersDao() {
		super(AppUser.class);
	}

	public Response getGroupMembers(long groupId, UriInfo uriInfo, int limit, int offset) {
		TypedQuery<AppUser> q = getEm().createNamedQuery("AppUser.membersOf", AppUser.class);
		q.setParameter("id",groupId);
		return executeQuery(q,uriInfo,limit,offset);
	}



}
