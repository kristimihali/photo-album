package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/groups")
public class GroupsController {

    // On  peut injecter le JWT
    /*@Inject
    private JsonWebToken callerPrincipal;*/

    /*@Inject
    @Claim(standard = Claims.raw_token)
    private String rawToken;*/

    /*@Inject
    @Claim(standard = Claims.upn)
    private ClaimValue<String> emailUPN;*/

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    //@RolesAllowed("admin")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.listAll(uriInfo, limit, offset);
    }

}
