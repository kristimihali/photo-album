package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Map;

@Path("/users")
public class UsersController {

    @Inject
    private GroupsDao groupsDao;

    @Inject
    private UsersDao usersDao;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(AppUser obj, @Context UriInfo uriInfo) {
        return usersDao.create(obj, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public AppUser read(@PathParam("id") long id) {
        return usersDao.read(id);
    }

    @PATCH
    @PUT
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Map<String, Object> obj, @Context UriInfo uriInfo) {
        return usersDao.update(id, obj, uriInfo);
    }

    @DELETE
    @Path("/{id: [0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") long id) {
        return usersDao.delete(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return usersDao.listAll(uriInfo, limit, offset);
    }

    /*@POST
    @Path("/{groupId: [0-9]+}/members")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMembers(List<Long> ids, @PathParam() long groupId, UriInfo uriInfo) {
        return groupsDao.addMembers(ids, groupId, uriInfo);
    }*/

    @POST
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createGroup(@PathParam("id") long userId, AppGroup g, @Context UriInfo uriInfo) {
        return groupsDao.createGroup(userId, g, uriInfo);
    }

    @GET
    @Path("/{id: [0-9]+}/groups")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGroups(@PathParam("id") long userId, @Context UriInfo uriInfo,
                              @PathParam("limit") int limit, @PathParam("offset") int offset) {
        return groupsDao.getGroups(userId, uriInfo, limit, offset);
    }
}
